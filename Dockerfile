FROM cern/cc7-base:latest # Use a tiny image

ENV RUNDECK_VERSION=2.6.9

RUN curl -o /root/rundeck.war http://download.rundeck.org/war/rundeck-${RUNDECK_VERSION}.war

RUN yum install -y zip unzip && yum clean all

# Workaround to add log4j.properties to the warfile, as required by WildFly
# ref. https://github.com/rundeck/rundeck/issues/1542
# As WildFly doesn't explode the WAR file but it uses them as they are, we have to
# manually explode the jar file, add the file and compress it back again
RUN mkdir -p /root/rundeck && unzip -d /root/rundeck /root/rundeck.war && \
    rm -f /root/rundeck.war && touch /root/rundeck/WEB-INF/classes/log4j.properties && \
    cd /root/rundeck &&  zip -r /root/rundeck.war * && cd /root/ && rm -rf /root/rundeck/
